from typing import List
import numpy as np
import torch
import matplotlib.pyplot as plt
import matplotlib
matplotlib.get_backend()
matplotlib.use('Agg')


def plot_loss(train_loss: List[float], valid_loss: List[float],
              output_path: str) -> None:
    """generate loss evolution accross epochs

    Parameters
    ----------

    train_loss
        list of train loss for each epochs
    valid_loss
        list of valid loss for each epochs
    output_path: str
        output figure file
    """

    epochs = np.arange(len(train_loss))
    # width = 0.35
    xsize = max(int(0.2 * len(train_loss)), 10)
    ysize = int(xsize * 9.0 / 16.0)
    fig, ax = plt.subplots(figsize=(xsize, ysize))
    plt.plot(epochs, train_loss, label="train loss")
    plt.plot(epochs, valid_loss, label="valid loss")
    ax.set_ylabel('loss')
    ax.set_title('loss by epochs for train and valid dataset')
    ax.set_xticks(epochs)
    ax.set_xticklabels([ep + 1 for ep in epochs], rotation=90)
    ax.legend()
    ax.grid()
    fig.tight_layout()
    plt.savefig(output_path)


def update_confusion_matrix(pred, targets, conf_matrix, all_preds,
                            all_targets):
    local_cm = torch.zeros_like(conf_matrix)
    decoded_predictions = torch.argmax(pred, 1)
    decoded_targets = torch.argmax(targets, 1)
    for t, p in zip(decoded_targets, decoded_predictions):
        local_cm[t, p] += 1
    return (conf_matrix + local_cm,
            torch.cat((all_preds.float(), decoded_predictions.float())),
            torch.cat((all_targets.float(), decoded_targets.float())))
