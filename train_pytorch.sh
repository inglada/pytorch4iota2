#!/bin/sh
#PBS -N pyti2
#PBS -q qgpgpu
#PBS -l select=1:ncpus=4:mem=92G:ngpus=1
#PBS -l walltime=12:00:00


if [ "$HOSTNAME" = pc-117-162 ]; then
    echo "$HOSTNAME"
    source ~/anaconda3/etc/profile.d/conda.sh
    conda activate iota2-env
    export SRCDIR=/home/inglada/Dev
    export MMDCDIR=${SRCDIR}/MMDC
    export PYTI2DIR=${SRCDIR}/PytorchIota2
    export IOTA2DIR=/home/inglada/Projects/THEIA_CESOSO/Dev/iota2
    export NUM_WORKERS=3
    export EPOCHS=100
    export BATCHSIZE=1000
    export USE_CLASS_WEIGHTS=0
    export LEARNING_RATE=0.01
    export EMBED_SIZE=128
    export INTERNAL_SIZE=32
    export DK=4
    export SAMPLEFILE_BN=lite.sqlite
    export SAMPLEFILE=${PYTI2DIR}/${SAMPLEFILE_BN}
    export OUTDIR=/tmp

else
    source "/work/scratch/inglada/miniconda3/etc/profile.d/conda.sh"
    export PATH="/work/scratch/inglada/miniconda3/bin:$PATH"
    conda activate iota2-torch
    export SRCDIR=/home/il/inglada/src/
    export IOTA2DIR=${SRCDIR}/iota2
    export MMDCDIR=${SRCDIR}/mmdc
    export PYTI2DIR=${SRCDIR}/pytorch4iota2
    export IOTA2DIR=${SRCDIR}/iota2
    export NUM_WORKERS=4
    export EPOCHS=50
    export BATCHSIZE=1000
    export USE_CLASS_WEIGHTS=0
    export LEARNING_RATE=0.01
    export EMBED_SIZE=256
    export INTERNAL_SIZE=128
    export DK=8
    # export SAMPLEFILE_BN=lite.sqlite
    export SAMPLEFILE_BN=Samples_region_1_seed0_learn.sqlite
    export SAMPLEFILE=/work/scratch/inglada/MMDC/${SAMPLEFILE_BN}
    export OUTDIR=/work/scratch/inglada/MMDC/temporal_reconstruction
    cp ${SAMPLEFILE} ${TMPDIR}/${SAMPLEFILE_BN}
    export SAMPLEFILE=${TMPDIR}/${SAMPLEFILE_BN}
fi


export DISAGNETDIR=${SRCDIR}/disagnet
export DASKHPCDIR=${SRCDIR}/daskhpc
export PYTHONPATH=${PYTHONPATH}:${PYTI2DIR}:${MMDCDIR}:${DISAGNETDIR}:${DASKHPCDIR}:${IOTA2DIR}

# MLP
# python ${PYTI2DIR}/train_pytorch.py ${SAMPLEFILE} ${OUTDIR}/model.txt ${PYTI2DIR}/features_list_4.txt ${NUM_WORKERS} ${EPOCHS} ${BATCHSIZE} M ${USE_CLASS_WEIGHTS} ${LEARNING_RATE} ${EMBED_SIZE} ${INTERNAL_SIZE} ${DK}
# SelfAtt
python ${PYTI2DIR}/train_pytorch.py ${SAMPLEFILE} ${OUTDIR}/model.txt ${PYTI2DIR}/features_list_4.txt ${NUM_WORKERS} ${EPOCHS} ${BATCHSIZE} S ${USE_CLASS_WEIGHTS} ${LEARNING_RATE} ${EMBED_SIZE} ${INTERNAL_SIZE} ${DK}
# # LTAE
# python ${PYTI2DIR}/train_pytorch.py ${SAMPLEFILE} ${OUTDIR}/model.txt ${PYTI2DIR}/features_list_4.txt ${NUM_WORKERS} ${EPOCHS} ${BATCHSIZE} L ${USE_CLASS_WEIGHTS} ${LEARNING_RATE} ${EMBED_SIZE} ${INTERNAL_SIZE} ${DK}


# ogr2ogr -dialect SQLITE -nln "output" -sql "SELECT * FROM output limit 50000" lite.sqlite /work/OT/theia/oso/arthur/runs/mini/DL_fast_v2/learningSamples/Samples_region_1_seed0_learn.sqlite
