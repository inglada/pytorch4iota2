#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import sys
import argparse
import logging
from tabulate import tabulate
from typing import List, Dict, Union, Callable, Optional
import torch
import torch.nn.functional as F
from iota2.Learning.pytorch.train_pytorch_model import (
    getFieldElement, doy_by_sensors, database_to_dataloader_batch_stream)
from sklearn.metrics import f1_score, accuracy_score, cohen_kappa_score
from Diagnostics import update_confusion_matrix, plot_loss
from iota2.Learning.pytorch.torch_nn_bank import (
    MLPClassifier, SimpleSelfAttentionClassifier, LTAEClassifier)

# Configure logging
numeric_level = getattr(logging, "INFO", None)
logging.basicConfig(level=numeric_level,
                    format="%(asctime)-15s %(levelname)s: %(message)s")
LOGGER = logging.getLogger(__name__)


class ConfusionMatrix_Loss(torch.nn.Module):
    '''Calculate F1 score. Can work with gpu tensors

    The original implmentation is written by Michal Haltuf on Kaggle.

    Adapted by J. Inglada to produce other metrics from http://arxiv.org/abs/2008.05756v1

    Returns
    -------
    torch.Tensor
        `ndim` == 1. epsilon <= val <= 1

    Reference
    ---------
    - https://www.kaggle.com/rejpalcz/best-loss-function-for-f1-score-metric
    - https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html#sklearn.metrics.f1_score
    - https://discuss.pytorch.org/t/calculating-precision-recall-and-f1-score-in-case-of-multi-label-classification/28265/6
    - http://www.ryanzhang.info/python/writing-your-own-loss-function-module-for-pytorch/
    '''
    def __init__(self, metric='f1', epsilon=1e-7):
        super().__init__()
        self.epsilon = epsilon
        self.metric = metric

    def tp_etc(self, y_pred, y_true, weight=None, batch_size=None):
        if weight is None:
            tp = (y_true * y_pred).sum(dim=0).to(torch.float32)
            tn = ((1 - y_true) * (1 - y_pred)).sum(dim=0).to(torch.float32)
            fp = ((1 - y_true) * y_pred).sum(dim=0).to(torch.float32)
            fn = (y_true * (1 - y_pred)).sum(dim=0).to(torch.float32)
        else:
            weight = weight / weight.mean() * batch_size
            tp = (y_true * y_pred * weight).sum(dim=0).to(torch.float32)
            tn = ((1 - y_true) * (1 - y_pred) * weight).sum(dim=0).to(
                torch.float32)
            fp = ((1 - y_true) * y_pred * weight).sum(dim=0).to(torch.float32)
            fn = (y_true * (1 - y_pred) * weight).sum(dim=0).to(torch.float32)
        return (tp, tn, fp, fn)

    def forward(self, y_pred, y_true, weight=None):
        # assert y_pred.ndim == 2
        # assert y_true.ndim == 1
        # y_true = F.one_hot(y_true, 2).to(torch.float32)
        # y_pred = F.softmax(y_pred, dim=1)
        batch_size = y_true.size()[0]

        if self.metric == 'f1':
            (tp, _, fp, fn) = self.tp_etc(y_pred, y_true, weight, batch_size)
            precision = tp / (tp + fp + self.epsilon)
            recall = tp / (tp + fn + self.epsilon)
            score = 2 * (precision * recall) / (precision + recall +
                                                self.epsilon)
        elif self.metric == 'acc':
            (tp, _, fp, fn) = self.tp_etc(y_pred, y_true, weight, batch_size)
            score = tp / (tp + fp + fn + self.epsilon)
        elif self.metric == 'k':
            tp = (y_true * y_pred).sum(dim=0).to(torch.float32)
            c = tp.sum()
            s = batch_size
            pk = y_pred.sum(dim=0)
            tk = y_true.sum(dim=0)
            ptk = (pk * tk).sum()
            kappa = (c * s - ptk) / (s * s - ptk + self.epsilon)
            return 1 - kappa
        elif self.metric == 'mcc':
            tp = (y_true * y_pred).sum(dim=0).to(torch.float32)
            c = tp.sum()
            s = batch_size
            pk = y_pred.sum(dim=0)
            tk = y_true.sum(dim=0)
            ptk = (pk * tk).sum()
            ppk = (pk * pk).sum()
            ttk = (tk * tk).sum()
            mcc = (c * s - ptk) / torch.sqrt((s * s - ppk) *
                                             (s * s - ttk) + self.epsilon)
            return 1 - mcc
        elif self.metric == 'compo':
            precision = tp / (tp + fp + self.epsilon)
            recall = tp / (tp + fn + self.epsilon)
            fscore = 2 * (precision * recall) / (precision + recall +
                                                 self.epsilon)
            po = tp / (tp + fp + fn + self.epsilon)
            pe = (tp + fp) / batch_size * fn / batch_size + (
                tp + fn) / batch_size * fp / batch_size
            kappa = (po - pe) / (1 - pe + self.epsilon)
            score = (fscore + po + kappa) / 3.0
        elif self.metric == 'ba':
            (tp, tn, fp, fn) = self.tp_etc(y_pred, y_true, weight, batch_size)
            tpr = tp / (tp + fn + self.epsilon)
            tnr = tn / (tn + fp + self.epsilon)
            score = (tpr + tnr) / 2.0
        elif self.metric == 'fm':
            score = torch.sqrt((1 / (1 + fp / (tp + self.epsilon))) *
                               (1 / (1 + fn / (tp + self.epsilon))))
        if weight is None:
            score = score.clamp(min=self.epsilon, max=1 - self.epsilon)
        return 1 - score.mean()


def str2bool(v: Union[str, bool]) -> bool:
    """ Convert a string representing a boolean value to a bool"""
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def torch_learn(dataset_path: str,
                features_labels: List[str],
                doys: Dict[str, List[int]],
                model_path: str,
                data_field: str,
                nn_name: Callable,
                normalization_factor: float,
                use_class_weights: bool = True,
                masks_labels: List[str] = [],
                epochs: int = 100,
                batch_size: int = 1000,
                learning_rate: float = 1e-3,
                embed_size=256,
                internal_size=128,
                dk=8,
                clip_gradient: Optional[float] = None,
                num_workers: int = 1,
                device=torch.device('cpu'),
                logger=LOGGER):
    """Train a neural network thanks to pytorch

    The resulting model is serialized and save thanks to pickle.

    Parameters
    ----------
    dataset_path
        input data base (SQLite format)
    features_labels
        columns name into the data base to consider in order to learn the model
    model_path
        output model path
    data_field
        label's data field in data set
    nn_name
        neural network name
    nn_parameters
        parameters as **dict to instaciate neural network object
    masks_labels
        mask column's name into the data base
    epochs
        number of epochs
    batch_size
        batch size
    num_workers
        number of parallel learning tasks
    logger
        root logger
    """

    logger.info(f"Using class weights {use_class_weights}")
    logger.info("Features use to build model : {}".format(features_labels))
    logger.info("Building the data loaders")
    (train_loader, valid_loader, _, _) = database_to_dataloader_batch_stream(
        dataset_path, data_field, features_labels,
        {"sentinel2": normalization_factor}, "originfid", masks_labels,
        batch_size, num_workers)

    nb_class = len(
        getFieldElement(dataset_path, "SQLite", data_field, "unique", "int"))
    logger.info(f"Found {nb_class} classes in the dataset")
    logger.info(f"There are {len(features_labels)} input features and "
                f"{len(masks_labels)} masks.")
    logger.info(
        f"Using {len(train_loader.dataset) * batch_size} training samples")
    logger.info(f"Batch size {batch_size}")
    logger.info(f"Embed size {embed_size}")
    logger.info(f"Internal size {internal_size}")
    logger.info(f"dk {dk}")
    logger.info(f"Gradient clipping: {clip_gradient}")

    # learn nn model
    model = nn_name(nb_features=len(features_labels),
                    nb_class=nb_class,
                    doy_sensors_dic=doys)
    if nn_name == LTAEClassifier:
        model = nn_name(nb_features=len(features_labels),
                        nb_class=nb_class,
                        doy_sensors_dic=doys,
                        embed_size=embed_size,
                        internal_size=internal_size,
                        dk=dk)
    model = model.to(device)
    f1_loss = ConfusionMatrix_Loss(metric='mcc').to(device)
    criterion = f1_loss  # F.binary_cross_entropy
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    train_loss = []
    valid_loss = []
    for i in range(epochs):
        print(f"epoch : {i+1}")
        train_loss_loader = []
        for j_train, (inputs, masks, targets,
                      weights) in enumerate(train_loader):
            # print(i, j_train)
            # print("Targets")
            # print(targets)
            # print("Features")
            # print(inputs)
            # print("Masks")
            # print(masks)
            # sys.exit()
            inputs = inputs.to(device, non_blocking=False).squeeze(0)
            masks = masks.to(device, non_blocking=False).squeeze(0)
            targets = targets.to(device, non_blocking=False).squeeze(0)
            params = {
                'l5': None,
                'l8': None,
                'l8_old': None,
                's2_theia': inputs,
                's2_s2c': None,
                's2_l3a': None,
                's1_desvv': None,
                's1_desvh': None,
                's1_ascvv': None,
                's1_ascvh': None,
                'l5_masks': None,
                'l8_masks': None,
                'l8_old_masks': None,
                's2_theia_masks': masks,
                's2_s2c_masks': None,
                's2_l3a_masks': None,
                's1_desvv_masks': None,
                's1_desvh_masks': None,
                's1_ascvv_masks': None,
                's1_ascvh_masks': None
            }
            y_hat = model(**params)
            if use_class_weights:
                weights = weights.to(device, non_blocking=False)
                loss = criterion(y_hat, targets, weight=weights)
            else:
                loss = criterion(y_hat, targets)
            optimizer.zero_grad()
            loss.backward()
            if clip_gradient is not None:
                torch.nn.utils.clip_grad_norm_(model.parameters(),
                                               clip_gradient)
            optimizer.step()
            train_loss_loader.append(loss.item())
        train_loss_mean = sum(train_loss_loader) / len(train_loss_loader)
        train_loss.append(train_loss_mean)
        logger.info(f"Epoch {i} train loss mean : {train_loss_mean:.4f}")
        with torch.no_grad():
            valid_loss_loader = []
            conf_matrix = torch.zeros(nb_class, nb_class, dtype=torch.int32)
            all_preds = torch.zeros(0)
            all_targets = torch.zeros(0)
            for _, (inputs, masks, targets_val,
                    weights_val) in enumerate(valid_loader):
                inputs = inputs.to(device, non_blocking=False).squeeze(0)
                masks = masks.to(device, non_blocking=False).squeeze(0)
                targets_val = targets_val.to(device,
                                             non_blocking=False).squeeze(0)
                params = {
                    'l5': None,
                    'l8': None,
                    'l8_old': None,
                    's2_theia': inputs,
                    's2_s2c': None,
                    's2_l3a': None,
                    's1_desvv': None,
                    's1_desvh': None,
                    's1_ascvv': None,
                    's1_ascvh': None,
                    'l5_masks': None,
                    'l8_masks': None,
                    'l8_old_masks': None,
                    's2_theia_masks': masks,
                    's2_s2c_masks': None,
                    's2_l3a_masks': None,
                    's1_desvv_masks': None,
                    's1_desvh_masks': None,
                    's1_ascvv_masks': None,
                    's1_ascvh_masks': None
                }
                pred = model(**params)
                # print("Targets")
                # print(targets)
                # print("Preds")
                # print(pred)
                # print(masks[1, :])
                # print(model.compute_attention_mask(masks)[1, :])
                # sys.exit()
                if use_class_weights:
                    weights_val = weights_val.to(device)
                    loss_pred = criterion(pred, targets_val, weights_val)
                else:
                    loss_pred = criterion(pred, targets_val)
                valid_loss_loader.append(loss_pred.item())
                conf_matrix, all_preds, all_targets = update_confusion_matrix(
                    pred.cpu(), targets_val.cpu(), conf_matrix, all_preds,
                    all_targets)
            valid_loss_mean = sum(valid_loss_loader) / len(valid_loss_loader)
            valid_loss.append(valid_loss_mean)
            logger.info(f"Epoch {i} valid loss mean : {valid_loss_mean:.4f}")
            logger.info(f"\n{tabulate(conf_matrix.numpy())}\n")
            f1score = f1_score(all_targets.numpy(),
                               all_preds.numpy(),
                               average='macro')
            oa = accuracy_score(all_targets.numpy(), all_preds.numpy())
            kappa = cohen_kappa_score(all_targets.numpy(), all_preds.numpy())
            logger.info(
                f"F1-score {f1score:.4f} - OA {oa:.4f} - Kappa {kappa:.4f}")
    torch.save(model.state_dict(), model_path)

    # save loss
    output_loss_figure = model_path.replace(".txt", "_loss.svg")
    plot_loss(train_loss, valid_loss, output_loss_figure)


if __name__ == '__main__':
    database_file = sys.argv[1]
    model_path = sys.argv[2]
    features_labels = sys.argv[3]
    num_workers = int(sys.argv[4])
    epochs = int(sys.argv[5])
    batch_size = int(sys.argv[6])
    normalization_factor = 10000.0
    data_field = "i2label"
    # MLPClassifier SimpleSelfAttentionClassifier  LTAEClassifier
    nn_name_arg = sys.argv[7]
    use_class_weights = str2bool(sys.argv[8])
    learning_rate = float(sys.argv[9])
    embed_size = int(sys.argv[10])
    internal_size = int(sys.argv[11])
    dk = int(sys.argv[12])
    LOGGER.info(f"Learning rate {learning_rate}")
    nn_name = LTAEClassifier
    if nn_name_arg == "M":
        nn_name = MLPClassifier
        LOGGER.info("MLP Classifier")
    elif nn_name_arg == "S":
        nn_name = SimpleSelfAttentionClassifier
        LOGGER.info("Simple Self Attention Classifier")
    else:
        LOGGER.info("LTE  Classifier")
    masks_labels = []

    features_labels_list = []
    with open(features_labels) as feat_file:
        for line in feat_file:
            lrst = line.rstrip()
            if 'mask' in lrst:
                masks_labels.append(lrst)
            else:
                features_labels_list.append(lrst)
    print(masks_labels)
    doys = doy_by_sensors(features_labels_list, LOGGER)
    print(doys)
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    LOGGER.info(f"Using device {device}")
    torch_learn(database_file,
                features_labels_list,
                doys,
                model_path,
                data_field,
                nn_name,
                normalization_factor,
                use_class_weights,
                masks_labels,
                epochs,
                batch_size,
                learning_rate,
                embed_size=embed_size,
                internal_size=internal_size,
                clip_gradient=None,
                dk=dk,
                num_workers=num_workers,
                device=device)
