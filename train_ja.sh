#!/bin/sh
#PBS -N ltaeps
#PBS -q qgpgpu
#PBS -J 1-512:1
#PBS -l select=1:ncpus=4:mem=92G:ngpus=1
#PBS -l walltime=12:00:00


source "/work/scratch/inglada/miniconda3/etc/profile.d/conda.sh"
export PATH="/work/scratch/inglada/miniconda3/bin:$PATH"
conda activate iota2-torch
export SRCDIR=/home/il/inglada/src/
export IOTA2DIR=${SRCDIR}/iota2
export MMDCDIR=${SRCDIR}/mmdc
export PYTI2DIR=${SRCDIR}/pytorch4iota2
export IOTA2DIR=${SRCDIR}/iota2
PARAM_FILE=${PYTI2DIR}/params_ltae.txt
PARAMS="$(sed -n ${PBS_ARRAY_INDEX}p "${PARAM_FILE}")"
export NUM_WORKERS=4
export EPOCHS=50
export BATCHSIZE=1000
export USE_CLASS_WEIGHTS=0
export LEARNING_RATE=0.01
export EMBED_SIZE=256
export INTERNAL_SIZE=128
export DK=8
export SAMPLEFILE_BN=Samples_region_1_seed0_learn.sqlite
export SAMPLEFILE=/work/scratch/inglada/MMDC/${SAMPLEFILE_BN}
export OUTDIR=/work/scratch/inglada/MMDC/temporal_reconstruction
cp ${SAMPLEFILE} ${TMPDIR}/${SAMPLEFILE_BN}
export SAMPLEFILE=${TMPDIR}/${SAMPLEFILE_BN}



export DISAGNETDIR=${SRCDIR}/disagnet
export DASKHPCDIR=${SRCDIR}/daskhpc
export PYTHONPATH=${PYTHONPATH}:${PYTI2DIR}:${MMDCDIR}:${DISAGNETDIR}:${DASKHPCDIR}:${IOTA2DIR}

python ${PYTI2DIR}/train_pytorch.py ${SAMPLEFILE} ${OUTDIR}/model.txt ${PYTI2DIR}/features_list_4.txt ${NUM_WORKERS} ${PARAMS}
